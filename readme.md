# Trabajo Backend
***
Nuestro cliente Burger King deesea realicemos un deashboard que permita la gestión de sus hamburguesas.

## Installation
***
A little intro about the installation. 
```
$ git clone https://gitlab.com/Jhonatanibericom/trabajo-backend
$ cd ../path/to/the/file
$ npm install
$ npm start
```

## ENDPOINTS
***
GET, POST, PATH, DELETE

$URL: http://localhost:5000/api/burgers

***
{
    "img": "http://bk-latam-prod.s3.amazonaws.com/sites/burgerking.com.pe/files/Crispy_300x270px.png",
    "name": "WHOPPER",
    "size": "Grande",
    "category": "GRANDES",
    "nutritionDetails": {
        "proteins": {
            "name": "Proteinas",
            "number": 36,
            "type": "g"
        },
        "calorys": {
            "name": "Calorias",
            "number": 920,
            "type": ""
        },
        "carbohydrates": {
            "name": "Carbohidratos",
            "number": 64,
            "type": "g"
        },
        "sugars": {
            "name": "Azúcares",
            "number": 47,
            "type": "g"
        },
        "fats": {
            "name": "Grasas",
            "number": 59,
            "type": "g"
        },
        "saturatedFats": {
            "name": "Grasas Saturadas",
            "number": 20,
            "type": "g"
        },
        "transFats": {
            "name": "Grasas Trans",
            "number": 0.5,
            "type": "g"
        },
        "sodium": {
            "name": "Sodio",
            "number": 0,
            "type": "mg"
        }
    }
}