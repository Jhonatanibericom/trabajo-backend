import Burger from '../models/Burger.model';
import { Response, Request } from 'express';
import { networkError, serverError, networkSuccess } from '../middlewares/response.middleware';
import { BurgerI } from '../domain/Burger.model';
import { NetworkResponseI } from '../domain/response.model';

const createBurger = async (request, response) => {
	try {
		const {name, size, img, category, price } = request.body as BurgerI;
		if(name && size && img && category && price){
			const burger = new Burger(request.body);
			await burger.save();
			response.status(201).send({
				success: true,
				message: 'Hamburguesa creada correctamente'
			});
			return;
		}
		return response.status(400).send({
			success:false,
			message: 'Datos invalidos'
		});
	} catch (error) {
		response.status(500).send({
			error,
			success:false,
			message: 'Ha ocurrido un problema'
		});
	}
}

const getAll = async (req: Request, res: Response) => {
	try {
		const dishes = await Burger.find();
		networkSuccess(res, dishes, 'Listado de hamburguesas');
	} catch (error) {
		serverError(res, error);
	}
}

const update = async (req: Request, res: Response) => {
	let response: NetworkResponseI;
	try {
		const burger = req.body;
		await Burger.updateOne({_id: burger._id}, {...burger});
		response = {
			data: null,
			message: 'Hamburguesa actualizado correctamente',
			success: true
		}
		res.send(response);
	} catch (error) {
		response = {
			success: false,
			message: 'Ha ocurrido un problema',
			error
		}
		res.status(500).send(response);
	}
}

const deleteBurger = async (req: Request, res: Response) => {
	let response: NetworkResponseI;
	try {
		const id = req.body._id;
		await Burger.deleteOne({_id: id});
		response = {
			success: true,
			message: 'Hamburguesa eliminada correctamente'
		};
		res.send(response);
	} catch (error) {
		response = {
			success: false,
			message: 'Ha ocurrido un problema',
			error
		}
		res.status(500).send(response);
	}
}


export { 
	createBurger, 
	getAll,
	update,
	deleteBurger
}