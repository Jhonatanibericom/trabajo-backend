// path - ruta : api/users
import { Router } from 'express';
import { createBurger, getAll, update, deleteBurger } from '../controllers/burger.controller';

const router = Router();

router.post('/', createBurger);
router.get('/', getAll);
router.patch('/', update)
router.delete('/', deleteBurger)

export default router;