import { Schema, model, SchemaTypes } from 'mongoose';

const BurgerSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	size: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	img: {
		type: String,
		required: true
	},
	category: {
		type: String,
		required: true
	},
	nutritionDetails: {
		type: SchemaTypes.Map
	}
});

export default model('hamburguesas', BurgerSchema);


